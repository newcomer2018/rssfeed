//
//  RSSDetailTableViewCell.swift
//  RSSFeedProject
//
//  Created by Umid on 20.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class RSSDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var rssImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
