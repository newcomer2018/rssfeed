//
//  RSSListTableViewCell.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class RSSListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
