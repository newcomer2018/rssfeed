//
//  ViewController.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIAdaptivePresentationControllerDelegate, UIPopoverPresentationControllerDelegate{

    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var addRSSButton: UIButton!
    
    var rssListArray = [RSSListObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        defaultSettings()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // Here we created 3 static RSS items and default settings tableView
    func defaultSettings(){
        
        
          let rt = RSSListObject.init(name: "RT News", link: "https://www.rt.com/rss/news/")
        
        let google = RSSListObject.init(name: "Google News", link: "https://news.google.com/rss/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNRGx1YlY4U0FtVnVHZ0pWVXlnQVAB?hl=en-US&gl=US&ceid=US:en")
      
        let  sputnik =  RSSListObject.init(name: "Sputnik", link: "https://sputniknews.com/export/rss2/world/index.xml")
        
        rssListArray = [rt, google, sputnik]
        
        addRSSButton.layer.cornerRadius = 10
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: "RSSListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "list")
        
        tableView.tableFooterView = UIView()
    }
    
    
    // open popub to add new RSS Feed to list
    @IBAction func addRSSAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let popub = storyboard.instantiateViewController(withIdentifier: "RSSPopubViewController") as! RSSPopubViewController
        popub.preferredContentSize = CGSize(width: self.view.frame.width, height:self.view.frame.height)
        popub.popoverPresentationController?.delegate = self
        popub.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        popub.modalPresentationStyle = .overCurrentContext
        popub.completionHand = { selectedItem in
            if !self.rssListArray.containsRSS(obj: selectedItem) {
                self.rssListArray.append(selectedItem)
                self.tableView.reloadData()
            }
        };
        present(popub, animated: true, completion: nil)
    }
    
    // MARK: - tableView
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssListArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "list", for: indexPath) as! RSSListTableViewCell
        let rssObject = rssListArray[indexPath.row]
        cell.titleLabel.text = rssObject.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
     let VC = storyboard.instantiateViewController(withIdentifier: "RSSViewController") as! RSSViewController
       let rss = rssListArray[indexPath.row]
       VC.selectedRSS = rss
      self.navigationController?.pushViewController(VC, animated: false)
        
    }
    // MARK - POPOver methods
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            rssListArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
}

extension Array {
    func containsRSS<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}
