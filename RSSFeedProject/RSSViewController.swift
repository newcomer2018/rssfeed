//
//  RSSViewController.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireRSSParser
class RSSViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var titleLabel:UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedRSS = RSSListObject.init(name: "", link: "")
    
    var rssItemArray = [RSSItem]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        defaultSettings()
        getRSSNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        titleLabel = UILabel.init(frame: CGRect(x: 20, y: 10, width: 200, height: 25))
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        let myAttribute = [
            NSAttributedString.Key.foregroundColor:UIColor.blue,
            NSAttributedString.Key.font: UIFont(name: "AppleSDGothicNeo-Regular", size: 20)!
        ]
        
        let myAttrString = NSAttributedString(string: selectedRSS.name, attributes: myAttribute)
        titleLabel.attributedText = myAttrString
        titleLabel.backgroundColor = UIColor.clear
         self.navigationItem.titleView = titleLabel
        
    }
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // get RSS Feed by selected RSS source
    func getRSSNews(){
        let url = selectedRSS.link
        Alamofire.request(url).responseRSS() { (response) -> Void in
            if let feed: RSSFeed = response.result.value {
                self.rssItemArray.removeAll()
                for item in feed.items {
                    self.rssItemArray.append(item)
                    print(item)
                }
            }
           
           self.tableView.reloadData()
        }
    }
    
    func defaultSettings(){
  
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: "RSSFeedTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "feed")
        
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - tableView
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssItemArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "feed", for: indexPath) as! RSSFeedTableViewCell
        let item = rssItemArray[indexPath.row]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd|HH:mm"
        let dateString = formatter.string(from: item.pubDate!)
        cell.titleLabel.text = item.title
        cell.dateLabel.text = dateString
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)

        let VC = storyboard.instantiateViewController(withIdentifier: "RSSDetailViewController") as! RSSDetailViewController
        let rss = rssItemArray[indexPath.row]
        VC.info = ["selectedItem":rss]
        self.navigationController?.pushViewController(VC, animated: false)
        
    }
}
