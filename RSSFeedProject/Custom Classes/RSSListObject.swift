//
//  RSSListObject.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class RSSListObject: Equatable {
    
    var name : String
    
    var link : String
    
    init( name: String, link: String ) {
        self.name = name
        self.link = link
    }
    static func ==(lhs:RSSListObject, rhs:RSSListObject) -> Bool { // Implement Equatable
        return lhs.name == rhs.name
    }
}
