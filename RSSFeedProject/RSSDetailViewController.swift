//
//  RSSDetailViewController.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit
import AlamofireRSSParser
import AlamofireImage
class RSSDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    var info = [String: RSSItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     defaultSettings()
    }
    
    func defaultSettings(){

        tableView.delegate = self
        tableView.dataSource = self

        let nib = UINib(nibName: "RSSDetailTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "detail")

        tableView.tableFooterView = UIView()
    }
    // MARK: - tableView
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! RSSDetailTableViewCell
        let selectedRSS = self.info["selectedItem"] as! RSSItem
       
        if selectedRSS.imagesFromDescription?.count != 0 {
            let imageUrl = selectedRSS.imagesFromDescription![0]
            let downloadURL = NSURL(string: imageUrl)!
            cell.rssImage.af_setImage(withURL: downloadURL as URL)
        }else {
           cell.rssImage.image = UIImage(named: "default")
        }
        
        
        cell.titleLabel.text = selectedRSS.title
        cell.descLabel.text = selectedRSS.itemDescription?.html2String
        return cell
    }
    

}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
