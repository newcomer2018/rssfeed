//
//  RSSPopubViewController.swift
//  RSSFeedProject
//
//  Created by Umid on 19.01.2019.
//  Copyright © 2019 Umid. All rights reserved.
//

import UIKit

class RSSPopubViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var sourceArray = [RSSListObject]()
    
    var nameArray = [String]()
    
    var linkArray = [String]()
    
    var selectedSource = RSSListObject.init(name: "", link: "")
    
open var completionHand: (( _ selectedItem: RSSListObject) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       
        defaultSetting()
    }
    
    func defaultSetting () {
        
        containerView.layer.cornerRadius = 10
        
        nameArray = ["The New York Times","CNN","Yahoo News"," Reuters","Washington Post"]
       // https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/world/rss.xml
        linkArray = ["https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/world/rss.xml",
        "http://rss.cnn.com/rss/edition_world.rss",
        "https://www.yahoo.com/news/rss/world",
        "http://feeds.reuters.com/Reuters/worldNews",
        "http://feeds.washingtonpost.com/rss/world"]
        
        for index in 0...nameArray.count - 1 {
            let name = nameArray[index]
            let link = linkArray[index]
            let source = RSSListObject.init(name: name, link: link)
            sourceArray.append(source)
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView()
        
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        
        let nib = UINib(nibName: "SourceTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "source")
            
        self.tableView.tableFooterView = UIView()
       
    }
    
    // MARK: - tableView methods
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "source", for: indexPath)as! SourceTableViewCell
        let souce = sourceArray[indexPath.row]
        cell.nameLabel.text = souce.name
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                if self.completionHand != nil {
                    self.selectedSource = self.sourceArray[indexPath.row]
                    self.completionHand!(self.selectedSource)
                }
            })
        }
    }
}
